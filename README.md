# Inventory Management API #

### How do I get set up? ###

* Summary of set up
  * Configuration
    * Clone project:
    * git clone https://drisal@bitbucket.org/drisal/inventory-management-api.git

* Dependencies
  * Build Dependencies:
    * ./gradlew build
  * Run Project 
    * ./gradlew run
* Database configuration
  * Using MySQL DB as docker container
    * docker-compose -f mysql-docker.yaml up -d
* How to run tests
  * ./gradlew test
* Deployment instructions
 * TODO:
