package com.example.inventory.management.api.model;

import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

@Data
@AllArgsConstructor
@ToString
@Entity
public class Inventory implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, updatable = false)
    private Long id;
    private String name;
    private String category;
    private String price;
    private String vendor;
    private String imageUrl;
    @Column(nullable = false, updatable = false)
    private String inventoryCode;

    public Inventory() {

    }
}
