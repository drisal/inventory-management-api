package com.example.inventory.management.api.persistence;

import com.example.inventory.management.api.model.Inventory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface InventoryDao extends JpaRepository<Inventory, Long> {
    void deleteInventoryById(Long id);
    Optional<Inventory> findInventoryById(Long id);
}
