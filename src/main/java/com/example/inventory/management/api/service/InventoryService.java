package com.example.inventory.management.api.service;

import com.example.inventory.management.api.exception.InventoryNotFoundException;
import com.example.inventory.management.api.model.Inventory;
import com.example.inventory.management.api.persistence.InventoryDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
public class InventoryService {

    private final InventoryDao inventoryDao;

    @Autowired
    public InventoryService(InventoryDao inventoryDao) {
        this.inventoryDao = inventoryDao;
    }

    @Transactional
    public Inventory addInventory(Inventory inventory){
        inventory.setInventoryCode(UUID.randomUUID().toString());
        return inventoryDao.save(inventory);
    }

    public List<Inventory> findAllInventory(){
        return inventoryDao.findAll();
    }

    public Inventory findInventoryById(Long id) {
        return inventoryDao.findInventoryById(id).orElseThrow(()-> new InventoryNotFoundException("Inventory by id: "+id+" was not found!!"));
    }

    @Transactional
    public Inventory updateInventory(Inventory inventory){
        return inventoryDao.save(inventory);
    }

    @Transactional
    public void deleteInventory(Long id){
        inventoryDao.deleteInventoryById(id);
    }
}
